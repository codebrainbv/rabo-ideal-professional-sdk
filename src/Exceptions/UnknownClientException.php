<?php

namespace CodebrainPpp\Hub\Exceptions;

class UnknownClientException extends ApiException
{
}

<?php

namespace CodebrainPpp\Hub\Http;

interface CodebrainHttpPickerInterface
{
    /**
     * @param \GuzzleHttp\ClientInterface|\CodebrainPpp\Hub\Http\CodebrainHttpPicker $httpClient
     *
     * @return \CodebrainPpp\Hub\Http\CodebrainHttpPicker
     */
    public function pickHttpAdapter($httpClient);
}

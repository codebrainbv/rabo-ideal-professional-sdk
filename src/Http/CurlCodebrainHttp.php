<?php

namespace CodebrainPpp\Hub\Http;

use CodebrainPpp\Hub\CodebrainPppApiClient;
use CodebrainPpp\Hub\Exceptions\ApiException;
use CodebrainPpp\Hub\Exceptions\CurlException;
use Composer\CaBundle\CaBundle;

final class CurlCodebrainHttp implements CodebrainHttpInterface
{
    /**
     * Default response timeout (in seconds).
     */
    public const DEFAULT_TIMEOUT = 10;

    /**
     * Default connect timeout (in seconds).
     */
    public const DEFAULT_CONNECT_TIMEOUT = 2;

    /**
     * HTTP status code for an empty ok response.
     */
    public const HTTP_NO_CONTENT = 204;

    /**
     * The maximum number of retries.
     */
    public const MAX_RETRIES = 5;

    /**
     * The amount of milliseconds the delay is being increased with on each retry.
     */
    public const DELAY_INCREASE_MS = 1000;

    /**
     * @param string $httpMethod
     * @param string $url
     * @param array  $headers
     * @param string $httpBody
     *
     * @return \stdClass|void|null
     *
     * @throws \CodebrainPpp\Hub\Exceptions\ApiException
     * @throws \CodebrainPpp\Hub\Exceptions\CurlException
     */
    public function send($httpMethod, $url, $headers, $httpBody)
    {
        for ($i = 0; $i <= self::MAX_RETRIES; ++$i) {
            usleep($i * self::DELAY_INCREASE_MS);

            try {
                return $this->attemptRequest($httpMethod, $url, $headers, $httpBody);
            } catch (CurlException $e) {
                throw new CurlException($e->getMessage());
            }
        }

        throw new CurlException('Unable to connect to the Codebrain HUB. Maximum number of retries ('.self::MAX_RETRIES.') reached.');
    }

    /**
     * @param string $httpMethod
     * @param string $url
     * @param array  $headers
     * @param string $httpBody
     *
     * @return \stdClass|void|null
     *
     * @throws \CodebrainPpp\Hub\Exceptions\ApiException
     */
    private function attemptRequest($httpMethod, $url, $headers, $httpBody)
    {
        $curl = curl_init($url);
        $headers['Content-Type'] = 'application/json';

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->parseHeaders($headers));
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, self::DEFAULT_CONNECT_TIMEOUT);
        curl_setopt($curl, CURLOPT_TIMEOUT, self::DEFAULT_TIMEOUT);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_CAINFO, CaBundle::getBundledCaBundlePath());

        switch ($httpMethod) {
            case CodebrainPppApiClient::HTTP_POST:
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $httpBody);

                break;
            case CodebrainPppApiClient::HTTP_GET:
                break;
            default:
                throw new \InvalidArgumentException('Invalid http method: '.$httpMethod);
        }

        $startTime = microtime(true);
        $response = curl_exec($curl);
        $endTime = microtime(true);

        if ($response === false) {
            $executionTime = $endTime - $startTime;
            $curlErrorNumber = curl_errno($curl);
            $curlErrorMessage = 'Curl error: '.curl_error($curl);

            if ($this->isConnectTimeoutError($curlErrorNumber, $executionTime)) {
                throw new CurlException('Unable to connect to the Codebrain HUB. '.$curlErrorMessage);
            }

            throw new ApiException($curlErrorMessage);
        }

        // extract header
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $headerSize);
        $headers = $this->getHeaders($header);

        // extract body
        $httpBody = substr($response, $headerSize);

        $statusCode = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
        curl_close($curl);

        return $this->parseResponseBody($response, $headers, $statusCode, $httpBody);
    }

    /**
     * The version number for the underlying http client, if available.
     *
     * @example Guzzle/7.7
     *
     * @return string|null
     */
    public function versionString()
    {
        return 'Curl/*';
    }

    /**
     * Whether this http adapter provides a debugging mode. If debugging mode is enabled, the
     * request will be included in the ApiException.
     *
     * @return false
     */
    public function supportsDebugging()
    {
        return false;
    }

    /**
     * @param int          $curlErrorNumber
     * @param string|float $executionTime
     *
     * @return bool
     */
    private function isConnectTimeoutError($curlErrorNumber, $executionTime)
    {
        $connectErrors = [
            \CURLE_COULDNT_RESOLVE_HOST => true,
            \CURLE_COULDNT_CONNECT => true,
            \CURLE_SSL_CONNECT_ERROR => true,
            \CURLE_GOT_NOTHING => true,
        ];

        if (isset($connectErrors[$curlErrorNumber])) {
            return true;
        }

        if ($curlErrorNumber === \CURLE_OPERATION_TIMEOUTED) {
            if ($executionTime > self::DEFAULT_TIMEOUT) {
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * @param string $response
     * @param array  $headers
     * @param int    $statusCode
     * @param string $httpBody
     *
     * @return \stdClass|null
     *
     * @throws \CodebrainPpp\Hub\Exceptions\ApiException
     */
    private function parseResponseBody($response, $headers, $statusCode, $httpBody)
    {
        if (empty($response)) {
            if ($statusCode === self::HTTP_NO_CONTENT) {
                return null;
            }

            throw new ApiException('No response body found.');
        }

        $object = new \stdClass();
        $object->body = @json_decode($httpBody);
        $object->headers = $headers;

        // Checks if the response is valid JSON
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new ApiException("Unable to decode Codebrain HUB response: '{$response}'.");
        }

        // Checks if the response has an error
        if (isset($object->body->error)) {
            throw new ApiException("Codebrain HUB error: '{$object->body->error}'.");
        }

        return $object;
    }

    private function parseHeaders($headers)
    {
        $result = [];

        foreach ($headers as $key => $value) {
            $result[] = $key.': '.$value;
        }

        return $result;
    }

    /**
     * Get the headers from the response.
     *
     * @param string $respHeaders
     *
     * @return array
     */
    private function getHeaders($respHeaders)
    {
        $headers = [];

        $headerText = substr($respHeaders, 0, strpos($respHeaders, "\r\n\r\n"));

        foreach (explode("\r\n", $headerText) as $i => $line) {
            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                list($key, $value) = explode(': ', $line);

                $headers[strtolower($key)] = $value;
            }
        }

        return $headers;
    }
}
